// Directions:
//Create two events for when a user types in the first and last name inputs
//when this event triggers, update the span-full-name's content to show the value of the first name input on the left and the value of the last name input on the right

//stretch goal: instead of an annonymous function, create 



let input1 = document.querySelector('#txt-first-name');
let input2 = document.querySelector('#txt-last-name');
let span1 = document.querySelector('#span-full-name');



input1.addEventListener('keyup', (e) => {
	myFunc(e)
})

input2.addEventListener('keyup', (e) => {
	myFunc(e)
})

function myFunc(e) {
	let firstName = input1.value;
	let lastName = input2.value;
	span1.innerHTML = `${firstName} ${lastName}`
}




